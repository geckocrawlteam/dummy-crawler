Dummy Crawler
-------------

This is a simple crawler we use for testing the platform. The crawler generates random data in table form.
It can also serve as an example for how to write crawlers, so please go ahead and check out the source code!

The inputs for the crawler:

* rows - How many rows of dummy data to generate.
* cols - How many columns of dummy data to generate.
* sleep - How many milliseconds to sleep between each row. This is useful for testing the progress bar.
* fail - If set, the crawler will fail with this error message. Useful for testing the error reporting.

Enjoy, and happy crawling!