function randchars() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (var i = 0; i < 8; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

function sleep(millis) {
	return new Promise((resolve, reject)=>{
		setTimeout(resolve,millis);
	});
}

module.exports=async function(crawl) {
	let result=[];

	if (!crawl.input.rows)
		crawl.input.rows=1;

	if (!crawl.input.cols)
		crawl.input.cols=1;

	if (crawl.input.cols>10)
		crawl.input.cols=10;

	crawl.log("Starting the dummy crawl...");

	for (let row=0; row<crawl.input.rows; row++) {
		let rowData={};
		for (let col=0; col<crawl.input.cols; col++)
			rowData["col"+(col+1)]=randchars();

		crawl.progress(100*row/crawl.input.rows);
		await sleep(crawl.input.sleep);

		crawl.result(rowData);
	}

	crawl.log("Dummy crawl complete!");

	if (crawl.input.fail && crawl.input.fail!="none" && crawl.input.fail!="no")
		throw new Error(crawl.input.fail);
};
